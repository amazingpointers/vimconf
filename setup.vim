" nvim-treesitter configs
lua << EOF
require'nvim-treesitter.configs'.setup{
    highlight = {
        enable = true
    },
    ensure_installed = "go",
    refactor = {
        highlight_definitions = {
            enable = true
        },
        highlight_current_scope = {
            enable = true
        },
    },
}
EOF
